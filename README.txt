Requires access to the BeanFive.com monitoring service. Sign up at 
http://beanfive.com.

Built-in support for:
  - Analytics
  - Performance checklist
  - SEO checklist
  - Security review & integrity check


Installation
------------

Copy the beanfive folder to your module directory and then enable on the admin
modules page along with all the dependancies. 

Author
------
Bryan Burkholder
http://beanfive.com
