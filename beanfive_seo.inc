<?php

/**
 * @file
 * SEO functions for Beanfive
 */

/*
 * Check if clean URLs is enabled
 */
function beanfive_cleanURLS() {
  return variable_get('clean_url');
}

/*
 * Return status of SEO modules
 */
function beanfive_seoModules() {
  return array(
    'path_auto' => module_exists('pathauto'),
    'global_redirect' => module_exists('globalredirect'),
    'google_analytics' => module_exists('googleanalytics'),
    'search404' => module_exists('search404'),
    'xml_sitemap' => module_exists('xmlsitemap'),
    'captcha' => module_exists('captcha'),
  );
}

/*
 * Check if Google Analytics value has been initialized
 */
function beanfive_googleAnalyticsConfigured() {
  if (variable_get('googleanalytics_account', 0) === 0) {
    return FALSE;
  }
  return TRUE;
}

/*
 * Check if robots.txt file exists
 *  @todo - include robots.txt validation
 */
function beanfive_checkRobots() {
  global $base_url;

  return (@fopen($base_url . '/robots.txt', "r") == TRUE);
}
