<?php

/**
 * @file
 * Performance functions for Beanfive
 */

/*
 * Returns the status of defualt Drupal css aggregation
 */
function beanfive_aggregateCSS() {
  return variable_get('preprocess_css', 0);
}

/*
 * Returns the status of default Drupal js aggregation
 */
function beanfive_aggregateJS() {
  return variable_get('preprocess_js', 0);
}

/*
 * Returns the status of default Drupal block caching
 */
function beanfive_cacheBlocks() {
  return variable_get('block_cache', FALSE);
}

/*
 * Returns the status of default Drupal anonymous caching
 */
function beanfive_cacheAnonymous() {
  return variable_get('cache', 0);
}

/*
 * Returns the minimum cache lifetime
 */
function beanfive_cacheMinLifetime() {
  return variable_get('cache_lifetime', 0);
}

/*
 * Returns the maximum cache lifetime
 */
function beanfive_cacheMaxLifetime() {
  return variable_get('page_cache_maximum_age', 0);
}

/*
 * Returns the status of default Drupal page compression
 */
function beanfive_pageCompression() {
  return variable_get('page_compression', TRUE);
}

/*
 * Checks the server to see if memcache has been installed and enabled
 */
function beanfive_memcacheEnabled() {
  if (class_exists('Memcache')) {
    return TRUE;
  }
  return FALSE;
}

/*
 * Returns the count of modules enabled, as indicated by the database
 */
function beanfive_moduleCount() {
  return db_query("SELECT COUNT(*) FROM {system} WHERE type = 'module' AND status = 1")->fetchField();
}

/*
 * Checks and returns the status of specific performance-degraging module
 */
function beanfive_performanceModulesDisabled() {
  return array(
    'migrate' => !module_exists('migrate'),
    'simpletest' => !module_exists('simpletest'),
    'coder' => !module_exists('coder'),
    'database_loging' => !module_exists('dblog'),
    'php_filter' => !module_exists('php'),
  );
}

/*
 * Checks the server for php max execution time
 */
function beanfive_maxExecutionTime() {
  return ini_get('max_execution_time');
}

/*
 * Checks the status of the php apc extention
 */
function beanfive_apcEnabled() {
  if (extension_loaded('apc')) {
    return '1';
  }
  elseif (extension_loaded('apc') && ini_get('apc.enabled')) {
    return '2';
  }
  else {
    return '0';
  }
}