<?php
/**
 * @file
 * Extra functions for Beanfive
 */

/*
 * Returns the module status array
 */
function beanfive_loadModuleStatus() {
  module_load_include('module', 'update', 'update');
  $available = update_get_available(TRUE);
  $data = update_calculate_project_data($available);
  return $data;
}

/*
 * Check to make sure the anonymous user record exists
 */
function beanfive_anonymousUser() {
  return db_query('SELECT COUNT(*) FROM {users} where uid =0')->fetchField();
}

/*
 * Determins if admin name is a common name, and returns true/false
 */
function beanfive_commonAdminName() {
  $name = db_query('SELECT name FROM {users} where uid = 1')->fetchField();
  $name = drupal_strtolower($name);
  $common_names = array('admin', 'administrator', 'root', 'adm', 'tech', 'user', 'manager', 'superuser', 'sys');

  if (in_array($name, $common_names)) {
    return TRUE;
  }
  return FALSE;
}

/*
 * Returns the status of display_errors from php settings
 */
function beanfive_phpErrorsEnabled() {
  return ini_get('display_errors');
}