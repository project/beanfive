<?php
/**
 * @file
 * Analytics functions for Beanfive
 */

/*
 * Encodes user data and returns formatted array
 */
function beanfive_userData() {
  $timestamp = time() - 900; //15 mins
  $user_data = array(
    'totalUsers' => db_query('SELECT COUNT(*) AS count FROM {users}')->fetchField() - 1, // to remove anonymous user
    'loggedInUsers' =>  db_query('SELECT COUNT(uid) FROM {sessions} WHERE uid <> 0 AND timestamp >=:time', array(':time' => $timestamp))->fetchField(),
    'anonymousUsers' => db_query('SELECT COUNT(hostname) FROM {sessions} WHERE timestamp >=:time AND uid = 0', array(':time' => $timestamp))->fetchField(),
  );

  return $user_data;
}


/*
 * Returns the total nodes in the system
 */
function beanfive_nodeData() {
  return db_query('SELECT COUNT(*) AS count FROM {node}')->fetchField();
}